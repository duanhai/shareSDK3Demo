//
//  AppDelegate.h
//  testSDK3
//
//  Created by Tony Duan on 9/27/16.
//  Copyright © 2016 ChengduRuiMa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

