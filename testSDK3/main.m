//
//  main.m
//  testSDK3
//
//  Created by Tony Duan on 9/27/16.
//  Copyright © 2016 ChengduRuiMa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
