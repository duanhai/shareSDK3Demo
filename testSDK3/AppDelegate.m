//
//  AppDelegate.m
//  testSDK3
//
//  Created by Tony Duan on 9/27/16.
//  Copyright © 2016 ChengduRuiMa. All rights reserved.
//

#import "AppDelegate.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>

//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

//微信SDK头文件
#import "WXApi.h"

//新浪微博SDK头文件
#import "WeiboSDK.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [ShareSDK registerApp:@"10fa329017127" activePlatforms:@[
                                                             @(SSDKPlatformTypeSinaWeibo),
                                                             @(SSDKPlatformTypeQQ),
                                                             @(SSDKPlatformTypeWechat),
                                                             @(SSDKPlatformTypeSMS),
                                                             @(SSDKPlatformTypeInstagram),
                                                             @(SSDKPlatformTypeTwitter),
                                                             @(SSDKPlatformTypeFacebook)
                                                             ] onImport:^(SSDKPlatformType platformType) {
                                                                 switch (platformType) {
                                                             
                                                                     case SSDKPlatformTypeWechat:
                                                                         [ShareSDKConnector connectWeChat:[WXApi class]];
                                                                         break;
                                                                     case SSDKPlatformTypeQQ:
                                                                         [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                                                                         break;
                                                                         
                                                                     default:
                                                                         break;
                                                                 }
                                                             }  onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
                                                                 //
                                                                 switch (platformType) {
                                                                         
                                                        
                                                                         
                                                                     case SSDKPlatformTypeSinaWeibo:
                                                                         //
                                                                         [appInfo SSDKSetupSinaWeiboByAppKey:@"3197202594" appSecret:@"72d92d19a5ea0bc832fd484965161620" redirectUri:@"http://remarkmedia.com" authType:SSDKAuthTypeBoth];
                                                                         break;
                                                                     case SSDKPlatformTypeWechat:
                                                                         [appInfo SSDKSetupWeChatByAppId:@"wx97efbb6dcf01e82b"
                                                                                               appSecret:@"7992d6a01b0a107f27567f051f6528a1"];
                                                                         
                                                                         break;
                                                                     case SSDKPlatformTypeQQ:
                                                                         [appInfo SSDKSetupQQByAppId:@"1104439887"
                                                                                              appKey:@"C7OwED5qjlmE5Jsh"
                                                                                            authType:SSDKAuthTypeBoth];
                                                                         break;
                                                                         
                                                                     case SSDKPlatformTypeInstagram:
                                                                         [appInfo SSDKSetupInstagramByClientID:@"8c1699681baa4f5eaaff21303f89f216" clientSecret:@"c49cfd1d983f4427a0d6f9809e731863" redirectUri:@"http://www.remarkmedia.com"];
                                                                         break;
                                                                         
                                                                     case SSDKPlatformTypeTwitter:
                                                                         [appInfo SSDKSetupTwitterByConsumerKey:@"RzPAAoFBy2PxzwdyWkAmWy0fH" consumerSecret:@"4L7BSgZgJ5y3LsECIo4PGrohzk11r0C1Vm5rq1Erm1Gh1UENfO" redirectUri:@"http://www.kankanapp.com.cn/"];
                                                                     break;
                                                                         
                                                                    case SSDKPlatformTypeFacebook:
                                                                         [appInfo SSDKSetupFacebookByApiKey:@"1803914373174974" appSecret:@"e0fb15c91df3493919bca43b30a0b267" authType:SSDKAuthTypeBoth];
                                                                         break;
                                                                     default:
                                                                         break;
                                                                 }
                                                             }];
    
    // Override point for customization after application launch.
    return YES;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
