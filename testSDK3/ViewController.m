//
//  ViewController.m
//  testSDK3
//
//  Created by Tony Duan on 9/27/16.
//  Copyright © 2016 ChengduRuiMa. All rights reserved.
//

#import "ViewController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>
@interface ViewController ()

@end

@implementation ViewController
- (IBAction)testLogin:(id)sender {
    
//    
//    [ShareSDK getUserInfo:SSDKPlatformTypeFacebook onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
//        //
//        
//    }];
    
    
//    [ShareSDK cancelAuthorize:SSDKPlatformTypeInstagram];
    
//    [ShareSDK cancelAuthorize:SSDKPlatformTypeInstagram];
    


    
//    [ShareSDK getUserInfo:SSDKPlatformTypeWechat
//           onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error)
//     {
//         if (state == SSDKResponseStateSuccess)
//         {
//             
//             NSLog(@"uid=%@",user.uid);
//             NSLog(@"%@",user.credential);
//             NSLog(@"token=%@",user.credential.token);
//             NSLog(@"nickname=%@",user.nickname);
//         }
//         
//         else
//         {
//             NSLog(@"%@",error);
//         }
//         
//     }];
//    
//    [SSEThirdPartyLoginHelper loginByPlatform:SSDKPlatformTypeQQ
//                                   onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
//                                       
//                                       //在此回调中可以将社交平台用户信息与自身用户系统进行绑定，最后使用一个唯一用户标识来关联此用户信息。
//                                       //在此示例中没有跟用户系统关联，则使用一个社交用户对应一个系统用户的方式。将社交用户的uid作为关联ID传入associateHandler。
//                                       associateHandler (user.uid, user, user);
//                                       NSLog(@"dd%@",user.rawData);
//                                       NSLog(@"dd%@",user.credential);
//                                       
//                                   }
//                                onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
//                                    
//                                    if (state == SSDKResponseStateSuccess)
//                                    {
//                                        
//                                    }
//                                    
//                                }];
//    return;
    //18084856005 pzh189 facebook
    
    //meizhouyanzu hhHH1314 Instagram
    
//    [shareParams SSDKSetupQQParamsByText:@"测试123" title:@"分享标题" url:[NSURL URLWithString:@"http://www.baidu.com"] thumbImage:[UIImage imageNamed:@"test"]  image:[UIImage imageNamed:@"test"] type:0 forPlatformSubType:SSDKPlatformTypeInstagram];
//    [shareParams SSDKSetupInstagramByImage:[UIImage imageNamed:@"test"] menuDisplayPoint:CGPointZero];
    
//    [shareParams SSDKSetupTwitterParamsByText:@"test" images:[UIImage imageNamed:@"test"] latitude:30 longitude:106.3 type:SSDKContentTypeText];
//    BOOL status = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]];
//   
//    [ShareSDK share:SSDKPlatformTypeTwitter parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
//        //
//    }];
    
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];

    
    [shareParams SSDKSetupInstagramByImage:@"http://www.mob.com/images/logo_black.png" menuDisplayPoint:CGPointZero];
    
    [shareParams SSDKEnableUseClientShare];

    [ShareSDK share:SSDKPlatformTypeInstagram parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        
        switch (state) {
            case SSDKResponseStateSuccess:
            {
               
                NSLog(@"分享成功");
                break;
            }
            case SSDKResponseStateFail:
            {
               
                NSLog(@"分享失败");
                break;
            }
            case SSDKResponseStateCancel:
            {
               
                NSLog(@"分享已取消");
                break;
            }
            default:
                break;
        }
    }];
    
}

- (IBAction)twitterShare:(id)sender {
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    
    [shareParams SSDKSetupTwitterParamsByText:@"Test Twitter Share" images:@"http://www.mob.com/images/logo_black.png" latitude:0 longitude:0 type:SSDKContentTypeImage];
    
    [shareParams SSDKEnableUseClientShare];
    
    [ShareSDK share:SSDKPlatformTypeTwitter parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                
                NSLog(@"分享成功");
                break;
            }
            case SSDKResponseStateFail:
            {
                
                
                NSLog(@"分享失败");
                break;
            }
            case SSDKResponseStateCancel:
            {
                
                NSLog(@"分享已取消");
                break;
            }
            default:
                break;
        }
    }];
    
    
}

- (IBAction)facebookShare:(id)sender {
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    
    
    [shareParams SSDKSetupFacebookParamsByText:@"Test Facebook Share" image:[UIImage imageNamed:@"test.jpg"] type:SSDKContentTypeImage];

    
    [shareParams SSDKEnableUseClientShare];
    
    [ShareSDK share:SSDKPlatformTypeFacebook parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                
                NSLog(@"分享成功");
                break;
            }
            case SSDKResponseStateFail:
            {
                
                
                NSLog(@"分享失败");
                break;
            }
            case SSDKResponseStateCancel:
            {
                
                NSLog(@"分享已取消");
                break;
            }
            default:
                break;
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
